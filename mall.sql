/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.29 : Database - xbmu
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`xbmu` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `xbmu`;

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `fakename` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

/*Data for the table `admin` */

insert  into `admin`(`id`,`name`,`fakename`,`sex`,`username`,`password`,`telephone`,`email`) values 
(36,'张三','法外狂徒','男','admin','admin','18375044558','23435783@qq.com'),
(48,'小王','小王','男','小王','123456','18375044559','45345534@qq.com'),
(49,'小丽','西厂皮卡丘','女','小丽','123456','18375044556','549279038@qq.com'),
(51,'李四','小李子','男','李四','123456','18375044557','23423476@qq.com');

/*Table structure for table `goods` */

DROP TABLE IF EXISTS `goods`;

CREATE TABLE `goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `in_price` double(8,2) DEFAULT NULL,
  `sale_price` double(8,2) DEFAULT NULL,
  `num` int(11) DEFAULT NULL,
  `discription` varchar(255) DEFAULT NULL,
  `discount` double(8,2) DEFAULT NULL,
  `product_place` varchar(255) DEFAULT NULL,
  `unit` varchar(255) DEFAULT NULL,
  `standard` varchar(255) DEFAULT NULL,
  `sale_num` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '0 下架 1  上架',
  `picture` varchar(255) DEFAULT NULL,
  `good_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `good_type` (`good_type`),
  CONSTRAINT `goods_ibfk_1` FOREIGN KEY (`good_type`) REFERENCES `goodstype` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

/*Data for the table `goods` */

insert  into `goods`(`id`,`name`,`in_price`,`sale_price`,`num`,`discription`,`discount`,`product_place`,`unit`,`standard`,`sale_num`,`status`,`picture`,`good_type`) values 
(36,'夏季短袖',150.00,180.00,100,'裙子夏季短袖新款2021年女装泡泡纱亮色格子淑女洋气休闲连衣裙女',1.00,'上海市优衣库公司','件','',0,1,'/files/nvzhuang1.jpg',11),
(37,'超大裙摆',100.00,120.00,100,'玉米高端2021年夏季浪漫蓝色桑蚕丝乔其收腰大摆灯笼袖真丝连衣裙',1.00,'榆中县优衣库分公司','件',NULL,0,0,'/files/nvzhuang2.jpg',11),
(38,'连衣裙女夏季',50.00,60.00,100,'lc老陈同学 白色连衣裙女夏季2021新款薄款泡泡袖长裙收腰v领裙子',NULL,'榆中县优衣库分公司','件',NULL,0,1,'/files/nvzhuang3.jpg',11),
(39,'西装领连衣裙',260.00,300.00,100,'美丽的皇帝 法式撞色西装领连衣裙女夏季新款高腰收腰显瘦中长裙',NULL,'榆中县优衣库分公司','件',NULL,0,0,'/files/nvzhuang4.jpg',11),
(40,'碎花连衣裙',280.00,300.00,360,'【ABOUT MIN 夏季新款】设计感系带温柔气质收腰碎花连衣裙上衣女',NULL,'上海市优衣库公司','件',NULL,0,1,'/files/nvzhuang5.jpg',11),
(41,'长袖衬衣男',50.00,60.00,233,'长袖衬衣男士春秋格子寸衫韩版潮流春季外套男装内搭港风日系衬衫',NULL,'榆中县优衣库分公司','件',NULL,0,1,'/files/nanzhuang1.jpg',12),
(42,'男装夏季运动圆领短袖',300.00,345.00,111,'阿迪达斯官网adidas三叶草 男装夏季运动圆领短袖T恤H09346H09347',NULL,'上海市优衣库公司','件',NULL,0,1,'/files/nanzhuang2.jpg',12),
(43,'GXG男装',197.00,200.00,100,'GXG男装【斯文系列】21年夏季热卖商务休闲基础纯色免烫短袖衬衫',NULL,'上海市优衣库公司','件',NULL,0,0,'/files/nanzhuang3.jpg',12),
(44,'男装短袖T恤',175.00,190.00,100,'英爵伦 230G重磅纯棉 夏季新款男装短袖T恤 圆领半袖百搭上衣体恤',NULL,'榆中县优衣库分公司','件',NULL,0,1,'/files/nanzhuang4.jpg',12),
(45,'圆领休闲宽松T恤',134.00,150.00,100,'WASSUP男装短袖T恤夏季新款幻影logo圆领休闲宽松女官方旗舰店',NULL,'榆中县优衣库分公司','件',NULL,0,0,'/files/nanzhuang5.jpg',12),
(46,'客厅沙发组合双人位',1000.00,1235.00,100,'华南家具简约现代全实木沙发新中式小户型客厅沙发组合双人位沙发',NULL,'北京市爱装家居','件',NULL,0,0,'/files/jiaju1.jpg',13),
(47,'推拉门走入式衣柜',2000.00,2300.00,100,'维意定制整体衣柜定制卧室推拉门走入式衣柜组合定做全屋家具定制',NULL,'北京市爱装家居','件',NULL,0,1,'/files/jiaju2.jpg',13),
(48,'现代简约整体衣柜',1400.00,14560.00,100,'尚品宅配现代简约整体衣柜定制卧室推拉门移门衣帽间全屋家具定做',NULL,'北京市爱装家居','件',NULL,0,1,'/files/jiaju3.jpg',13),
(49,'儿童床',1540.00,1700.00,100,'儿童房家具组合套装青少年儿童床男孩单人床1.5米储物高箱小孩床',NULL,'榆中县','件',NULL,0,1,'/files/jiaju4.jpg',13),
(50,'电视柜茶几',1500.00,1700.00,100,'北欧风可伸缩电视柜茶几组合简约现代客厅储物柜多功能小户型地柜',NULL,'榆中县','件',NULL,0,0,'/files/jiaju5.jpg',13),
(52,'皮箱子小型旅行箱',200.00,245.00,100,'结实耐用学生行李箱男24密码皮箱子小型旅行箱女万向轮20寸拉杆箱',NULL,'榆中县','件',NULL,0,1,'/files/xiangbao1.jpg',14),
(53,'行李箱女小皮箱',120.00,145.00,100,'不莱玫行李箱女小皮箱大容静音万向轮轻便拉杆箱子旅行箱20寸登机',NULL,'榆中县','件',NULL,0,1,'/files/xiangbao2.jpg',14),
(54,'ins网红新款密码箱',76.00,87.00,100,'24行李箱女小型轻便20寸学生ins网红新款密码箱子26旅行拉杆箱潮',NULL,'兰州市','件',NULL,0,1,'/files/xiangbao3.jpg',14),
(55,'商务行李箱',135.00,145.00,100,'萨蒙斯商务行李箱男女20寸登机箱子24大容量万向轮小型旅行拉杆箱',NULL,'兰州市','件',NULL,0,1,'/files/xiangbao4.jpg',14),
(56,'儿童麦咭拉杆箱',120.00,140.00,100,'儿童麦咭拉杆箱新款 蛋黄哥联名休闲斜跨腰包网红百搭胸包懒蛋蛋',NULL,'兰州市','件',NULL,0,1,'/files/xiangbao5.jpg',14),
(57,'婴儿背带腰凳',300.00,329.00,100,'抱抱熊婴儿背带腰凳轻便四季多功能宝宝抱娃神器背娃前后两用夏季',NULL,'西安市','件',NULL,0,1,'/files/muying1.jpg',15),
(58,'生宝宝衣服玩具',112.00,120.00,100,'婴儿用品大全新生儿礼盒初生刚出生宝宝衣服玩具满月礼物套装母婴',NULL,'青海息宁','件',NULL,0,1,'/files/muying2.jpg',15),
(59,'贝亲PPSU奶瓶',97.00,100.00,100,'贝亲PPSU奶瓶新生婴儿宝宝宽口径奶瓶塑料耐摔吸管正品1岁2岁以上',NULL,'贵州贵阳','件',NULL,0,1,'/files/muying3.jpg',15),
(60,'新生儿婴儿床',200.00,210.00,100,'医院婴儿车豪华气控升降母婴月子会所中心新生儿婴儿床高透明医用',NULL,'吉林长春','件',NULL,0,0,'/files/muying4.jpg',15),
(61,'满月见面礼物套装母婴',70.00,78.00,100,'婴儿用品大全新生儿礼盒刚出生宝宝玩具初生满月见面礼物套装母婴',NULL,'山东菏泽曹县','件',NULL,0,1,'/files/muying5.jpg',15);

/*Table structure for table `goodstype` */

DROP TABLE IF EXISTS `goodstype`;

CREATE TABLE `goodstype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typename` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

/*Data for the table `goodstype` */

insert  into `goodstype`(`id`,`typename`) values 
(11,'女装'),
(12,'男装'),
(13,'家居'),
(14,'箱包'),
(15,'母婴'),
(16,'童装'),
(17,'家电'),
(18,'数码'),
(19,'手机'),
(20,'图书影像'),
(21,'医药保健'),
(22,'手表'),
(23,'珠宝饰品'),
(24,'文具'),
(25,'零食'),
(26,'茶酒'),
(27,'运动户外'),
(28,'美妆'),
(29,'礼品鲜花'),
(30,'农资绿植'),
(31,'汽车用品'),
(32,'宠物'),
(33,'洗漱用品');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
