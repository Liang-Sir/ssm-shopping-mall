package com.xmd.web.service;

import java.util.List;

import com.xmd.web.entity.Goods;

public interface GoodsService {

	
	List<Goods> getAllGoodsAdmin();
	
	void toGoodsSave(Goods goods);

	void toDown(Integer id);

	void toUp(Integer id);

	void deleteGoods(Integer id);

	Goods seleteById(Integer id);

	void EditGoodsSave(Goods goods);

	String getPicPath(Integer id);

	void edutGoodsPic(Integer id, String path);

	List<Goods> seleteByTypeName(Integer id);

	List<Goods> seleteByValue(String SValue);

	List<Goods> getAllGoodsView();



}
