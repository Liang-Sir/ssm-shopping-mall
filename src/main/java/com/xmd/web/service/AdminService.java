package com.xmd.web.service;

import java.util.List;

import com.xmd.web.entity.Admin;

public interface AdminService {
	public Admin getUsernameAndPassword(String username,String password);

	public List getAllAdmins();

	public boolean deleteAdmin(Integer id);

	public void addAdmin(Admin admin);

	public Admin findAdminid(Integer id);

	public int editAdminSave(Admin admin);


}
