package com.xmd.web.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.xmd.web.entity.Goodstype;


public interface GoodsTypeService{

	List<Goodstype> getAlltypes();

	void deleteGoodsType(Integer id);

	void addGoodsTypeSave(Goodstype goodstype);

	Goodstype findGoodsType(Integer id);

	int editGoodsTypeSave(Goodstype goodstype);

	Goodstype getTypeName(Integer id);

}
