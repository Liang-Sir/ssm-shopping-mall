package com.xmd.web.service.Impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xmd.web.dao.AdminMapper;
import com.xmd.web.entity.Admin;
import com.xmd.web.service.AdminService;
@Service
public class AdminServiceImpl implements AdminService{
	@Autowired
	private AdminMapper adminMapper;
	@Override
	public Admin getUsernameAndPassword(String username,String password) {
		Map<String,Object> param = new HashMap<String, Object>();
		param.put("username", username);
		param.put("password", password);
		
		return adminMapper.selectByUsernameAndPassword(param);
	}
	@Override
	public List getAllAdmins() {
		// TODO Auto-generated method stub
		return adminMapper.selectAllAdmin();
	}
	@Override
	public boolean deleteAdmin(Integer id) {
		// TODO Auto-generated method stub
		int del = adminMapper.deleteByPrimaryKey(id);
		if(del>0){
            return true;
        }
        return false;
	}
	@Override
	public void addAdmin(Admin admin) {
		// TODO Auto-generated method stub
		adminMapper.insert(admin);
	}
	@Override
	public Admin findAdminid(Integer id) {
		// TODO Auto-generated method stub
		return adminMapper.selectByPrimaryKey(id);
	}
	@Override
	public int editAdminSave(Admin admin) {
		// TODO Auto-generated method stub
		return adminMapper.updateByPrimaryKey(admin);
	}

	
}
