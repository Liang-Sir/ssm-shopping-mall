package com.xmd.web.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xmd.web.dao.GoodsMapper;
import com.xmd.web.dao.GoodstypeMapper;
import com.xmd.web.entity.Goodstype;
import com.xmd.web.service.GoodsTypeService;
@Service
public class GoodsTypeServiceImpl implements GoodsTypeService{
	@Autowired
	private GoodstypeMapper goodsTypeMapper;
	@Autowired
	private GoodsMapper goodsMapper;
	@Override
	public List<Goodstype> getAlltypes() {
		// TODO Auto-generated method stub
		return goodsTypeMapper.selectAllTypes();
	}
	@Transactional
	@Override
	public void deleteGoodsType(Integer id) {
		// TODO Auto-generated method stub
		goodsMapper.setGoodTypeAsNull(id);
		goodsTypeMapper.deleteByPrimaryKey(id);
	}
	@Override
	public void addGoodsTypeSave(Goodstype goodstype) {
		// TODO Auto-generated method stub
		goodsTypeMapper.insert(goodstype);
	}
	@Override
	public Goodstype findGoodsType(Integer id) {
		// TODO Auto-generated method stub
		return goodsTypeMapper.selectByPrimaryKey(id);
	}
	@Override
	public int editGoodsTypeSave(Goodstype goodstype) {
		// TODO Auto-generated method stub
		return goodsTypeMapper.updateByPrimaryKey(goodstype);
	}
	@Override
	public Goodstype getTypeName(Integer id) {
		// TODO Auto-generated method stub
		return goodsTypeMapper.selectByPrimaryKey(id);
	}
	
}
