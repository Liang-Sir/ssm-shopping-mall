package com.xmd.web.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xmd.web.dao.GoodsMapper;
import com.xmd.web.entity.Goods;
import com.xmd.web.service.GoodsService;
@Transactional
@Service
public class GoodsServiceImpl implements GoodsService{
	@Autowired
	private GoodsMapper goodsMapper;

	@Override
	public void toGoodsSave(Goods goods) {
		// TODO Auto-generated method stub
		goodsMapper.insert(goods);
	}
	@Override
	public void toDown(Integer id) {
		// TODO Auto-generated method stub
		goodsMapper.setStatusDown(id);
	}
	@Override
	public void toUp(Integer id) {
		// TODO Auto-generated method stub
		goodsMapper.setStatusUp(id);
	}
	@Override
	public void deleteGoods(Integer id) {
		// TODO Auto-generated method stub
		goodsMapper.deleteByPrimaryKey(id);
	}
	@Override
	public Goods seleteById(Integer id) {
		// TODO Auto-generated method stub
		return goodsMapper.selectByPrimaryKey(id);
	}
	@Override
	public void EditGoodsSave(Goods goods) {
		// TODO Auto-generated method stub
		goodsMapper.updateByPrimaryKey(goods);
	}
	@Override
	public String getPicPath(Integer id) {
		
		return goodsMapper.getPicPath(id);
		// TODO Auto-generated method stub
		
	}
	@Override
	public void edutGoodsPic(Integer id, String path) {
		// TODO Auto-generated method stub
		goodsMapper.editGoodsPic(id,path);
	}
	@Override
	public List<Goods> seleteByTypeName(Integer id) {
		// TODO Auto-generated method stub
		return goodsMapper.seleteByTypeName(id);
	}
	@Override
	public List<Goods> seleteByValue(String SValue) {
		// TODO Auto-generated method stub
		return goodsMapper.seleteByValue(SValue);
	}
	@Override
	public List<Goods> getAllGoodsAdmin() {
		// TODO Auto-generated method stub
		return goodsMapper.getAllGoods();
	}
	@Override
	public List<Goods> getAllGoodsView() {
		// TODO Auto-generated method stub
		return goodsMapper.getAllGoodsStatus();
	}


}
