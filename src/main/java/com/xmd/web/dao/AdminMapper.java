package com.xmd.web.dao;

import java.util.List;
import java.util.Map;

import com.xmd.web.entity.Admin;

public interface AdminMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Admin record);

    int insertSelective(Admin record);

    Admin selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Admin record);

    int updateByPrimaryKey(Admin record);

	Admin selectByUsernameAndPassword(Map<String, Object> param);

	List selectAllAdmin();

}