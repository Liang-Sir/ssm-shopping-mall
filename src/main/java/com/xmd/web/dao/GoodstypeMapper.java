package com.xmd.web.dao;

import java.util.List;

import com.xmd.web.entity.Goodstype;

public interface GoodstypeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Goodstype record);

    int insertSelective(Goodstype record);

    Goodstype selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Goodstype record);

    int updateByPrimaryKey(Goodstype record);

	List<Goodstype> selectAllTypes();
}