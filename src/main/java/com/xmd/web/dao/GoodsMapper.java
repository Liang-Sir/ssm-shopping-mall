package com.xmd.web.dao;

import java.util.List;

import com.xmd.web.entity.Goods;

public interface GoodsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Goods record);

    int insertSelective(Goods record);

    Goods selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Goods record);

    int updateByPrimaryKey(Goods record);

	void setGoodTypeAsNull(Integer id);

	List<Goods> getAllGoods();

	void setStatusDown(Integer id);

	void setStatusUp(Integer id);

	String getPicPath(Integer id);

	void editGoodsPic(Integer id, String path);

	List<Goods> seleteByTypeName(Integer id);

	List<Goods> seleteByValue(String SValue);

	List<Goods> getAllGoodsStatus();

}