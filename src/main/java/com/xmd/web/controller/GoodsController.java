package com.xmd.web.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import com.xmd.web.entity.Goods;
import com.xmd.web.entity.Goodstype;
import com.xmd.web.service.GoodsService;
import com.xmd.web.service.GoodsTypeService;
import com.xmd.web.utils.FileUploadUtil;

@Controller
@RequestMapping("/admin")
public class GoodsController {
	@Autowired
	private GoodsService goodsService;
	@Autowired
	private GoodsTypeService goodsTypeService;
	@RequestMapping("/goods/getAllGoods")
	public String getAllGoods(Map<String, Object> map) {
		List<Goods> goodsList = goodsService.getAllGoodsAdmin();
		map.put("goodsList", goodsList);
		return "admin/goods/goods_list";
	}
	@RequestMapping("/goods/toAdd")
	public String toAdd(Map<String, Object> map) {
		List<Goodstype> typeList = goodsTypeService.getAlltypes();
		map.put("typeList",typeList);
		return "admin/goods/goods_add";
	}
	@RequestMapping("/goods/toGoodsSave")
	public String toGoodsSave(Goods goods,Map<String, Object> map) {
		goodsService.toGoodsSave(goods);
		map.put("message", "商品添加成功！！！");
		return "forward:/admin/goods/getAllGoods";
	}
	@RequestMapping("/goods/toDown")
	public String toDown(Integer id,Map<String, Object> map) {
		goodsService.toDown(id);
		map.put("message", "商品下架成功！！！");
		return "forward:/admin/goods/getAllGoods";
	}
	@RequestMapping("/goods/toUp")
	public String toUp(Integer id,Map<String, Object> map) {
		goodsService.toUp(id);
		map.put("message", "商品上架成功！！！");
		return "forward:/admin/goods/getAllGoods";
	}
	@RequestMapping("/goods/deleteGoods")
	public String deleteGoods(Integer id,Map<String, Object> map) {
		goodsService.deleteGoods(id);
		map.put("message", "商品删除成功！！！");
		return "forward:/admin/goods/getAllGoods";
	}
	@RequestMapping("/goods/toEditGoods")
	public String toEditGoods(Integer id,Map<String, Object> map) {
		//查goods
		Goods goods = goodsService.seleteById(id);
		//查goodstype
		List<Goodstype> typeList = goodsTypeService.getAlltypes();
		map.put("goods", goods);
		map.put("typeList", typeList);
		return "/admin/goods/goods_edit";
	}
	@RequestMapping("/goods/toEditGoodsSave")
	public String toEditGoodsSave(Goods goods,Map<String, Object> map) {
		goodsService.EditGoodsSave(goods);
		map.put("message", "商品修改成功！！！");
		return "forward:/admin/goods/getAllGoods";
	}
	@RequestMapping("/goods/toUpload")
	public String toUpload(Integer id,Map<String, Object> map) {
		String path = goodsService.getPicPath(id);
		map.put("path", path);
		map.put("id", id);
		return "/admin/goods/goods_pic_edit";
	}
	@RequestMapping("/goods/editGoodsPic")
	public String editGoodsPic(Integer id,MultipartFile picture,HttpServletRequest request,Map<String, Object> map) {
		String path = FileUploadUtil.upload(picture,request);
		goodsService.edutGoodsPic(id,path);
		map.put("message", "上传成功！！！");
		return "forward:/admin/goods/getAllGoods";
	}
}
