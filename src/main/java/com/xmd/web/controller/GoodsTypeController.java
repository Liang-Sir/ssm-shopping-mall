package com.xmd.web.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.xmd.web.entity.Goods;
import com.xmd.web.entity.Goodstype;
import com.xmd.web.service.GoodsTypeService;

@Controller
@RequestMapping("/admin/goodstype")
public class GoodsTypeController {
	@Autowired
	private GoodsTypeService goodsTypeService;
	@RequestMapping("/getAllTypes")
	public String getAllTypes(Map<String,Object> map) {
		List<Goodstype> typeList = goodsTypeService.getAlltypes();
		map.put("typeList", typeList);
		return "admin/goodstype/goodstype_list";
	}
	@RequestMapping("/deleteGoodsType")
	public String deleteGoodsType(Integer id,Map<String,Object> map) {
		goodsTypeService.deleteGoodsType(id);
		map.put("message", "商品分类删除成功！");
		return "forward:/admin/goodstype/getAllTypes";
	}
	@RequestMapping("/toAddGoodsType")
	public String toAddGoodsType() {
		return "admin/goodstype/goodsType_add";
	}
	@RequestMapping("/addGoodsTypeSave")
	public String addGoodsTypeSave(Goodstype goodstype,Map<String,Object> map) {
		goodsTypeService.addGoodsTypeSave(goodstype);
		map.put("message", "商品分类添加成功！");
		return "forward:/admin/goodstype/getAllTypes";
	}
	@RequestMapping("/toEditGoodsType")
	public String toEditGoodsType(Integer id,Map<String,Object> map) {
		Goodstype goodsTypes = goodsTypeService.findGoodsType(id);
		map.put("goodsTypes", goodsTypes);
		return "admin/goodstype/goodstype_edit";
	}
	@RequestMapping("/editGoodsTypeSave")
	public String editGoodsTypeSave(Goodstype goodstype,Map<String,Object> map) {
		goodsTypeService.editGoodsTypeSave(goodstype);
		map.put("message", "商品分类修改成功！");
		return "forward:/admin/goodstype/getAllTypes";
	}
}
