package com.xmd.web.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.request.SessionScope;
import org.springframework.web.servlet.ModelAndView;

import com.xmd.web.entity.Admin;
import com.xmd.web.service.AdminService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
@Controller
@RequestMapping("/admin")
@SessionAttributes({"error","admin"})
public class AdminController {
	@Autowired
	private AdminService adminService;
	@RequestMapping("/toLogin")
	public String toLogin() {
		return "admin/admin_login";
	}
	@RequestMapping("/login")
	public String login(String username,String password,Map<String,Object> map) {
		Admin admin = adminService.getUsernameAndPassword(username, password);
		if(admin != null && admin.getName() != null) {
			map.put("admin",admin);
			return "admin/index";
		}else {
			map.put("error","用户名或密码不正确！！！");
			return "redirect:/admin/toLogin";
		}
		
	}
	@RequestMapping("/getIndexv3")
	public String getIndexv3() {
		return "admin/index_v3";
	}
	@RequestMapping("/getAllAdmins")
	public String getAllAdmins(Map<String,Object> map) {
		List<Admin> adminList =  adminService.getAllAdmins();
		map.put("adminList", adminList);
		return "admin/admin/admin_list";
	}
	/*
	 * @RequestMapping("deleteAdmin") public String deleteAdmin(id) { return null; }
	 */
	@RequestMapping("/deleteAdmin")
	public String deleteAdmin(Integer id,Map<String,Object> map) {
		boolean del = adminService.deleteAdmin(id);
		 if(del){
			 map.put("message", "删除成功！");
			 return "forward:/admin/getAllAdmins";
	        }else
	        return "admin/admin/shibai";
	}
	@RequestMapping("/toAdd")
	public String toAdd() {
		return "admin/admin/admin_add";
	}
	@RequestMapping("/addAdminSave")
	public String addAdmin(Admin admin,Map<String,Object> map) {
		adminService.addAdmin(admin);
		map.put("message","添加成功！");
		return "forward:/admin/toAdd";
	}
	@RequestMapping("/toEdit")
	public String toEdit(Integer id,Map<String,Object> map) {
		Admin adminId = adminService.findAdminid(id);
		map.put("adminId", adminId);
		return "admin/admin/admin_edit";
	}
	@RequestMapping("/editAdminSave")
	public String editAdminSave(Admin admin,Map<String,Object> map) {
		adminService.editAdminSave(admin);
		map.put("message", "修改成功！");
		return "forward:/admin/getAllAdmins";
	}
	@RequestMapping("/Quit")
	public String Quit(HttpServletRequest request, HttpServletResponse response) {
		
		//request.getSession().invalidate();
		HttpSession session = request.getSession();
		//session.removeAttribute("admin");  
		session.invalidate();
		return "admin/admin_login";
	}
	@GetMapping("/Quit")
	public ModelAndView getLogout(HttpSession session) {
		System.out.println("session: " + session);
		System.out.println("session id: " + session.getId());    
		session.invalidate();    
		ModelAndView mv = new ModelAndView();
		mv.setViewName("redirect:/index.jsp");
		return mv;
		}
	
}
