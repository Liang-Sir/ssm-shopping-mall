package com.xmd.web.controller;

import java.util.List;
import java.util.Map;

import javax.swing.plaf.PanelUI;

import org.apache.ibatis.reflection.ParamNameUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.xmd.web.entity.Goods;
import com.xmd.web.entity.Goodstype;
import com.xmd.web.service.GoodsService;
import com.xmd.web.service.GoodsTypeService;
@Controller
@RequestMapping("/mall")
@SessionAttributes({"typeList","goodsOne","goodsList"})
public class MallController {
	@Autowired
	private GoodsTypeService goodsTypeService;
	@Autowired
	private GoodsService goodsService;
	@RequestMapping("/index")
	public String getAllGoodsTypes(Map<String, Object> map) {
		List<Goodstype> typeList = goodsTypeService.getAlltypes();
		List<Goods> goodsList = goodsService.getAllGoodsView();
		map.put("typeList", typeList);
		map.put("goodsList", goodsList);
		return "mall/product";
	}
	@RequestMapping("/getTypes")
	public String getTypes(Map<String, Object> map) {
		List<Goodstype> typeList = goodsTypeService.getAlltypes();
		map.put("typeList", typeList);
		return "forward:/mall/getAllGoods";
	}
	@RequestMapping("/findGoodsType")
	public String findGoodsType(Integer id,Map<String , Object> map) {
		List<Goods> goods = goodsService.seleteByTypeName(id);
		Goodstype goodstypes = goodsTypeService.getTypeName(id);
		List<Goodstype> typeList = goodsTypeService.getAlltypes();
		map.put("typeList", typeList);
		map.put("goodstypes", goodstypes);
		map.put("goods", goods);
		return "/mall/product_find_list";
	}
	@RequestMapping("Introduce")
	public String Introduce(Integer id,Map<String, Object> map) {
		Goods goodsOne = goodsService.seleteById(id);
		map.put("goodsOne", goodsOne);
		return "/mall/product_introduce";
	}
	@RequestMapping("/toSearch")
	public String toSearch(String SValue,Map<String, Object> map) {
		List<Goods> goodsSearch = goodsService.seleteByValue(SValue);
		map.put("goodsSearch", goodsSearch);
		map.put("SValue", SValue);
		return "/mall/product_search";
	}
}
