package com.xmd.web.entity;

public class Goods {
    private Integer id;

    private String name;

    private Double inPrice;

    private Double salePrice;

    private Integer num;

    private String discription;

    private Double discount;

    private String productPlace;

    private String unit;

    private String standard;

    private Integer saleNum;

    private Integer status;
    
    private String picture;
    
    public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	private Goodstype type;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getInPrice() {
		return inPrice;
	}

	public void setInPrice(Double inPrice) {
		this.inPrice = inPrice;
	}

	public Double getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(Double salePrice) {
		this.salePrice = salePrice;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public String getDiscription() {
		return discription;
	}

	public void setDiscription(String discription) {
		this.discription = discription;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public String getProductPlace() {
		return productPlace;
	}

	public void setProductPlace(String productPlace) {
		this.productPlace = productPlace;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public Integer getSaleNum() {
		return saleNum;
	}

	public void setSaleNum(Integer saleNum) {
		this.saleNum = saleNum;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Goodstype getType() {
		return type;
	}

	public void setType(Goodstype type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Goods [id=" + id + ", name=" + name + ", inPrice=" + inPrice + ", salePrice=" + salePrice + ", num="
				+ num + ", discription=" + discription + ", discount=" + discount + ", productPlace=" + productPlace
				+ ", unit=" + unit + ", standard=" + standard + ", saleNum=" + saleNum + ", status=" + status
				+ ", picture=" + picture + ", type=" + type + "]";
	}

	
    
}