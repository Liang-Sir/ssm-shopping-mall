package com.xmd.web.entity;

public class Goodstype {
    private Integer id;

    private String typename;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTypename() {
        return typename;
    }

    @Override
	public String toString() {
		return "Goodstype [id=" + id + ", typename=" + typename + "]";
	}

	public void setTypename(String typename) {
        this.typename = typename == null ? null : typename.trim();
    }
}