<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<%=request.getServletContext().getContextPath() %>/static/mall/css/index_style.css">
    <script type="text/javascript" src="<%=request.getServletContext().getContextPath() %>/static/mall/js/index.js"></script>

    <title>商品分类查询</title>
    <style type="text/css">
  	li:hover {
	background-color: #EEEEEE;
}
.goods_i{
	width: 70%;
	height: 600px;
	margin: 0 auto;
	background-color: #EEEEEE;
}
.goods_i div{
	display: inline-block;
	float: left;
}
.goods_i .goods_i_1{
	width: 40%;
	height: 500px;
	margin-top:50px;
	background-color: green;
}
.goods_i .goods_i_2{
	width: 60%;
	height: 500px;
	margin-top:50px;
}
.goods_i .goods_i_1 img{
	width: 100%;
	height: 500px;
}
.goods_i_2 p,span,h1,button{
	margin-left: 100px;
	margin-top: 33px;
}
.jieshao{
	display: inline-block;
	font-size: 20px;
}
.goods_i_2 button{
	width: 150px;
	height: 40px;
	font-size: 20px;
}
  </style>
</head>
<script type="text/javascript">
	
</script>
<body>
    <!-- 头部部分开始 -->
    <div class="header">

        <!-- 头部中间部分的列表 -->
        <div class="header_middle">
            <div class="w">
                <ul>
                    <li><a href="#">欢迎来到生活品商城！！！</a></li>
                    <li style="display:inline-block;margin-left: 750px" class="right"><a href="<%=request.getServletContext().getContextPath() %>/admin/toLogin">登录后台</a></li>
                    <li style="margin-left: 25px"><a href="message.html" >个人中心</a></li>
                    <li class="shopping" style="float: right"><a href="shopping.html">购物车(0)</a></li>
                </ul>
            </div>
        </div>
        <!-- 头部logo部分 -->
        <div class="top_logo w">
            <a href="#" class="logo"></a>
            <ul class="list" id="list">
                <li><a href="<%=request.getServletContext().getContextPath() %>/index.jsp">商城首页</a></li>
            </ul>
           <a href="#" class="search_btn" id="search_btn"></a>
            <div class="search">
                <input type="search" value="" class="search-text" id="search_text">
            </div>

        </div>
        <!-- 头部app部分 -->
        <div class="top_app w">  
            <!-- 轮播图效果 start -->
            <div id="box_autoplay">
                <div class="box_autoplay_list">
                    <ul>
                        <li><img src="<%=request.getServletContext().getContextPath() %>/static/mall/images/app_banner.webp.jpg" style="width: 1226px; height: 460px;" /></li>
                        <li><img src="<%=request.getServletContext().getContextPath() %>/static/mall/images/app_banner.webp2.jpg" style="width: 1226px; height: 460px;" /></li>
                        <li><img src="<%=request.getServletContext().getContextPath() %>/static/mall/images/app_banner.webp3.webp.jpg" style="width: 1226px; height: 470px;"/></li>
                        <li><img src="<%=request.getServletContext().getContextPath() %>/static/mall/images/app_banner.webp4.webp.jpg" style="width: 1226px; height: 470px;" /></li>
                    </ul>
                </div>
            </div>
            <!-- 轮播图效果 end -->
            <div class="app_list">
                <ul>
                   <c:forEach items="${typeList }" var="v">
                   		<li><a href="#" onclick="toFind(${v.id})">${v.typename }</a></li>
                   </c:forEach>
                </ul>
            </div>
        </div>

    </div>
    <!-- 头部部分结束 -->
            <!-- 手机模块 -->
        <div class="phone w">
            <div class="part1">
                <h2>商品详情页</h2>
            </div>
           
        </div>
         	<div class="goods_i">
           		<%-- <ul id="goods_list">
           			
           				<li>
							<img alt="" src="<%=request.getServletContext().getContextPath() %>${goodsOne.picture }">
	           				<h4>${goodsOne.name }</h4>
	           				<span>${goodsOne.discription }</span>
	           				<p><a style="color: red;font-size: 18px">￥${goodsOne.inPrice }/${goodsOne.unit }</a>&nbsp;&nbsp;&nbsp;&nbsp;<s style="color:grey">￥${goodsOne.salePrice }</s></p>
	           				<p style="font-size: 12px;margin-top: 10px;">所属分类：${goodsOne.type.typename }</p>
           				</li>
           			
           		</ul> --%>
           		<div class="goods_i_1">
           			<img alt="" src="<%=request.getServletContext().getContextPath() %>${goodsOne.picture }">
           		</div>
           		<div class="goods_i_2">
           			<h1>${goodsOne.name }</h1>
           			<span class="jieshao">${goodsOne.discription }</span>
	           		<p>价格：<a style="color: red;font-size: 18px">￥${goodsOne.inPrice }/${goodsOne.unit }</a>&nbsp;&nbsp;&nbsp;&nbsp;<s style="color:grey">￥${goodsOne.salePrice }</s></p>
           			<p class="guige">规格:&nbsp;&nbsp;&nbsp;${goodsOne.unit }</p>
           			<p class="chandi">产地:&nbsp;&nbsp;&nbsp;${goodsOne.productPlace }</p>
           			<p class="kucun">库存:&nbsp;&nbsp;&nbsp;${goodsOne.num }${goodsOne.unit }</p>
           			<button style="background-color: #FFE4D0;">立即购买</button>
           			<button style="background-color: #F40;">加入购物车</button>
           		</div>
           	</div>
    <!-- 结尾部分开始 -->
    <div class="footer" style="display: inline-block;height: 200px">
        <!-- 服务模块 -->
       
            <div class="tool_footer" style="margin-left: 150px">
                    <ul class="col_links" style="margin-left: 250px">
                        <h3>帮助中心</h3>
                        <li><a href="#"> 账户管理</a></li>
                        <li><a href="#"> 购物指南</a></li>
                        <li><a href="#"> 订单操作</a></li>
                    </ul>
                    <ul class="col_links">
                        <h3>服务支持</h3>
                        <li><a href="#"> 售后政策 </a></li>
                        <li><a href="#"> 自助服务</a></li>
                        <li><a href="#"> 相关下载</a></li>
                    </ul>
                    <ul class="col_links">
                        <h3>线下门店</h3>
                        <li><a href="#"> 商城之家</a></li>
                        <li><a href="#"> 服务网点</a></li>
                        <li><a href="#"> 授权体验店</a></li>
                    </ul>
                    <ul class="col_links">
                        <h3>关于我们</h3>
                        <li><a href="#"> 了解我们</a></li>
                        <li><a href="#"> 加入我们</a></li>
                    </ul>
                    <ul class="col_links">
                        <h3>关注我们</h3>
                        <li><a href="#"> 新浪微博</a></li>
                        <li><a href="#"> 官方微信</a></li>
                        <li><a href="#"> 联系我们</a></li>
                    </ul>
            </div>
        </div>
       <div class="info middle ">
            <div class="info_header w">

                <div class="info_logo_text" style="text-align: center">
                    <p class="info_logo_text1" style="display:inline-block;margin-top: 20px">
                        Copyright © 2021 生活品商城. All rights reserved.
                    </p>
                    <p class="info_logo_text1" style="margin-top: 5px">
                        <a href="#" style="font-size: 16px">京ICP备XXXXXXXX号-2</a>
                    </p>
                </div>
            </div>

        </div>
        
        <script type="text/javascript">
        	function toFind(id){
        		location.href="<%=request.getServletContext().getContextPath() %>/mall/findGoodsType?id="+id;
        	}
        	var btn = document.getElementById("search_btn");
        	var text = document.getElementById("search_text");
        	btn.onclick = function(){
        		location.href="<%=request.getServletContext().getContextPath() %>/mall/toSearch?SValue="+text.value;
        	}
        </script>
</body>

</html>