<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    

    <title>商品分类信息列表</title>
    
    <link rel="shortcut icon" href="favicon.ico"> <link href="<%=request.getServletContext().getContextPath() %>/static/css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
    <link href="<%=request.getServletContext().getContextPath() %>/static/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">

    <!-- Data Tables -->
    <link href="<%=request.getServletContext().getContextPath() %>/static/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

    <link href="<%=request.getServletContext().getContextPath() %>/static/css/animate.min.css" rel="stylesheet">
    <link href="<%=request.getServletContext().getContextPath() %>/static/css/style.min.css?v=4.0.0" rel="stylesheet">

</head>
<script type="text/javascript">
	<c:if test="${not empty message }">
		alert("${message}");
	</c:if>
</script>
<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>商品分类信息列表</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="table_data_tables.html#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="table_data_tables.html#">选项1</a>
                                </li>
                                <li><a href="table_data_tables.html#">选项2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                                <tr>
                                    <th>商品分类ID</th>
                                    <th>商品分类名称</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                            <tbody>
	                            <c:forEach items="${typeList }" var="v">
	                             	<tr class="gradeX">
	                                    <td>${v.id }</td>
	                                    <td>${v.typename }</td>
	                                  
	                                    <td>
	                                     	<button class="btn btn-success btn-sm" onclick="edit(${v.id })" style="margin-left: 20px">修改</button>
	                        			 	<button class="btn btn-danger btn-sm" onclick="deleteAdmin(${v.id })" style="margin-left: 20px">删除</button>
	                                    </td>
	                                </tr>
								</c:forEach>
                            </tbody>
                            
                        </table>

                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <script src="<%=request.getServletContext().getContextPath() %>/static/js/jquery.min.js?v=2.1.4"></script>
    <script src="<%=request.getServletContext().getContextPath() %>/static/js/bootstrap.min.js?v=3.3.5"></script>
    <script src="<%=request.getServletContext().getContextPath() %>/static/js/plugins/jeditable/jquery.jeditable.js"></script>
    <script src="<%=request.getServletContext().getContextPath() %>/static/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<%=request.getServletContext().getContextPath() %>/static/js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script src="<%=request.getServletContext().getContextPath() %>/static/js/content.min.js?v=1.0.0"></script>
    <script>
        $(document).ready(function(){$(".dataTables-example").dataTable();var oTable=$("#editable").dataTable();oTable.$("td").editable("../example_ajax.php",{"callback":function(sValue,y){var aPos=oTable.fnGetPosition(this);oTable.fnUpdate(sValue,aPos[0],aPos[1])},"submitdata":function(value,settings){return{"row_id":this.parentNode.getAttribute("id"),"column":oTable.fnGetPosition(this)[2]}},"width":"90%","height":"100%"})});function fnClickAddRow(){$("#editable").dataTable().fnAddData(["Custom row","New row","New row","New row","New row"])};
    </script>
    <script type="text/javascript" src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>
	<script type="text/javascript">
		function deleteAdmin(id){
			var flag = confirm("你确定要删除吗?");
			if(flag){
				location.href= "<%=request.getServletContext().getContextPath() %>/admin/goodstype/deleteGoodsType?id="+id;
			}
		}
		function edit(id){
			location.href= "<%=request.getServletContext().getContextPath() %>/admin/goodstype/toEditGoodsType?id="+id;

		}
	</script>
</body>

</html>
