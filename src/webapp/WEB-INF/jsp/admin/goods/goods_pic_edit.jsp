<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    

    <title>上传商品缩略图</title>
   
    <link rel="shortcut icon" href="favicon.ico"> <link href="<%=request.getServletContext().getContextPath() %>/static/css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
    <link href="<%=request.getServletContext().getContextPath() %>/static/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="<%=request.getServletContext().getContextPath() %>/static/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<%=request.getServletContext().getContextPath() %>/static/css/animate.min.css" rel="stylesheet">
    <link href="<%=request.getServletContext().getContextPath() %>/static/css/style.min.css?v=4.0.0" rel="stylesheet">

</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>上传商品缩略图</h5>
                       
                    </div>
                    <div class="ibox-content">
                        <form method="post" class="form-horizontal" action="<%=request.getServletContext().getContextPath() %>/admin/goods/editGoodsPic"
                        enctype="multipart/form-data">
                             <div class="form-group">
                                <label class="col-sm-2 control-label">商品ID</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="id" value="${id}" readonly="readonly"></input>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">上传缩略图</label>

                                <div class="col-sm-10">
                                    <input type="file" class="form-control" name="picture"></input>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">上传</button>
                                    <input class="btn btn-white" type="reset" value="重置"></input>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="ibox-content">
                   		 <img style="width: 300px; height: 300px;" alt="商品缩略图" src="<%=request.getServletContext().getContextPath() %>${path}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<%=request.getServletContext().getContextPath() %>/static/js/jquery.min.js?v=2.1.4"></script>
    <script src="<%=request.getServletContext().getContextPath() %>/static/js/bootstrap.min.js?v=3.3.5"></script>
    <script src="<%=request.getServletContext().getContextPath() %>/static/js/content.min.js?v=1.0.0"></script>
    <script src="<%=request.getServletContext().getContextPath() %>/static/js/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
    </script>
    <script type="text/javascript" src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>
</body>

</html>