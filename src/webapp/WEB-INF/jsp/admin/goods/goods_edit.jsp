<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    

    <title>修改商品信息</title>
  
    <link rel="shortcut icon" href="favicon.ico"> 
    <link href="<%=request.getServletContext().getContextPath() %>/static/css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
    <link href="<%=request.getServletContext().getContextPath() %>/static/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="<%=request.getServletContext().getContextPath() %>/static/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<%=request.getServletContext().getContextPath() %>/static/css/animate.min.css" rel="stylesheet">
    <link href="<%=request.getServletContext().getContextPath() %>/static/css/style.min.css?v=4.0.0" rel="stylesheet">

</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
       
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                       <h5>修改商品信息</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="form_basic.html#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="form_basic.html#">选项1</a>
                                </li>
                                <li><a href="form_basic.html#">选项2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form method="post" class="form-horizontal"  action="<%=request.getServletContext().getContextPath() %>/admin/goods/toEditGoodsSave">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">商品ID:</label>
                                <div class="col-sm-10">
                                    <input readonly="readonly" type="text" name="id" class="form-control" value="${goods.id }">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">商品名称:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control" value="${goods.name }">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">商品进价:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="inPrice" class="form-control" value="${goods.inPrice }">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">商品售价:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="salePrice" class="form-control" value="${goods.salePrice }">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">商品数量:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="${goods.num }" name="num">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">商品简介:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="${goods.discription }" name="discription">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">商品折扣:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="${goods.discount }" name="discount">
                                </div>
                            </div>
                              <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">商品产地:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="productPlace" value="${goods.productPlace }" class="form-control">
                                </div>
                            </div>
                              <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">商品单位:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="unit" value="${goods.unit }" class="form-control">
                                </div>
                            </div>
                              <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">商品含量:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="standard" value="${goods.standard }" class="form-control">
                                </div>
                            </div>
                              <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">商品销售量:</label>
                                <div class="col-sm-10">
                                    <input readonly="readonly" type="text" name="saleNum" value="${goods.saleNum }" class="form-control">
                                </div>
                            </div>
                              <div class="hr-line-dashed"></div>
                               <div class="form-group">
                                <label class="col-sm-2 control-label">商品上下架情况:</label>
                                <div class="col-sm-10">
                                	<c:if test="${goods.status == 1 }">
                                		<input readonly="readonly" type="text" name="status" value="${goods.status }" class="form-control">已上架</input>
                                	</c:if>
                                    <c:if test="${goods.status == 0 }">
                                		<input readonly="readonly" type="text" name="status" value="${goods.status }" class="form-control">已下架</input>
                                	</c:if>
                                </div>
                            </div>
                         
                              <div class="hr-line-dashed"></div>
                           <div class="hr-line-dashed"></div>
                           
                            <div class="form-group">
                                <label class="col-sm-2 control-label">商品类型：</label>
                                <div class="col-sm-10">
                                    <select name="type.id" class="form-control m-b">
                                    	<option>请选择</option>
                                    	<c:forEach items="${typeList }" var="v"> 
                                    		<c:choose>
                                    			<c:when test="${v.id == goods.type.id }">
                                    				<option selected="selected" value="${v.id }">${v.typename }</option>
                                    			</c:when>
                                    			<c:otherwise>
                                    				<option value="${v.id }">${v.typename }</option>
                                    			</c:otherwise>
                                    		</c:choose>
                                    	</c:forEach>
                                    </select>
                                </div>
                            </div>                        
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2" style="margin: 0 auto;text-align:center">
                                    <button class="btn  btn-primary" type="submit">保存修改内容</button>
                                    <input class="btn  btn-warning" type="reset" value="重置" />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <script src="<%=request.getServletContext().getContextPath() %>/static/js/jquery.min.js?v=2.1.4"></script>
    <script src="<%=request.getServletContext().getContextPath() %>/static/js/bootstrap.min.js?v=3.3.5"></script>
    <script src="<%=request.getServletContext().getContextPath() %>/static/js/content.min.js?v=1.0.0"></script>
    <script src="<%=request.getServletContext().getContextPath() %>/static/js/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
    </script>
    <script type="text/javascript" src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>
</body>

</html>