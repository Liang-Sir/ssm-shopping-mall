<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<%=request.getServletContext().getContextPath() %>/static/mall/css/index_style.css">
    <script type="text/javascript" src="<%=request.getServletContext().getContextPath() %>/static/mall/js/index.js"></script>

    <title>生活品商城-首页</title>
   
</head>
<script type="text/javascript">
	function init(){
		location.href="<%=request.getServletContext().getContextPath() %>/mall/index";
	}
	init();
</script>
<body>
</body>
</html>